<?php 
require './class/db.php';
require './class/model.php';
require './class/tables.php';
require './class/system.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<!-- bootstrap css -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<!-- Jquery, Jquery slim, Popper & Bootstrap -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery-3.4.1.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script>window.Popper || document.write('<script src="assets/js/popper-1.14.7.min.js"><\/script>')</script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script>if(typeof($.fn.modal) === 'undefined') {document.write('<script src="assets/js/bs4.3.1//bootstrap.min.js"><\/script>')}</script>

    <!-- notify -->
    <!-- <script src="./assets/js/bootstrap-notify.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mouse0270-bootstrap-notify/3.1.7/bootstrap-notify.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css"/>

    <!-- icheck -->
    <!-- <script src="plugins/iCheck/icheck.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <!-- <link rel="stylesheet" href="plugins/iCheck/all.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/minimal/blue.css" />

    <!-- SELECT2 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>

    <!-- core js -->
    <script src="./includes/core.js" crossorigin="anonymous"></script>

</head>
<body>
	<div class="container mt-3">

		<!-- start a link action -->
		<h5>a link action</h5>
		<a class='btn btn-primary action' url='system.php'
		data = '{"action":"link","id":"$id"}' href='#' >a link</a>
		<!-- end a link action -->

		<hr>

		<!-- start modal show -->
		<h5>Modal Show</h5>
		<a class='btn btn-primary' onclick="modalShow('system.php',{action:'modalShow',id:'$id'})" href='#' >Modal Show</a>
		<!-- end modal show -->

		<hr>

		<!-- start a open json modal -->
		<h5>json Modal</h5>
		<a class='btn btn-primary action' url='system.php'
		data = '{"action":"jsonModal","id":"$id"}' href='#' >json modal</a>
		<!-- end a open json modal -->

		<hr>
		
		<!-- start data table -->
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
		<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

		<h5>DataTable</h5>
		<section class="bg-blue-05 py-6">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-12">
						<div class="shadow bg-white p-5">
							<h2>Test</h2>
							<div class="line-shape bg-blue mb-5"></div>
							<?php systemClass::testTable() ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- end data table -->

		<hr>

		<!-- start form -->
		<h5>Form</h5>
		<section class="bg-blue-05">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-8">
						<div class="border shadow rounded bg-white p-5">
							<span class="fa fa-key f-400 mb-3"></span>
							<h2>Form</h2>
							<?= systemClass::formAdd() ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- end form -->
	</div>





<!-- modal1 -->
  <div class="data-modal modal fade" id="modal1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="d-block">
          <button type="button" class="close pb-1 px-1 m-2 border shadow" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body p-3">
          <div class="text-center">
            <div class="spinner-border text-primary"></div>
            <div class="text-secondary">Loading...</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END modal1 -->

  <!-- modal2 -->
  <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
      <div class="modal-content shadow">
        <div class="d-block">
          <button type="button" class="close pb-1 px-1 m-2 border shadow" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body p-3">
            <div class="text-center">
              <div class="spinner-border text-primary"></div>
              <div class="text-secondary">Loading...</div>
            </div>
        </div>
      </div>
      </div>
  </div>
  <!-- END modal2 -->

  <!-- modal1 Lock-->
  <div class="data-modal modal fade" role="dialog" id="modal-lock" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content" >
          <div class="modal-body p-3">
              <div class="text-center">
                <div class="spinner-border text-primary"></div>
                <div class="text-secondary">Loading...</div>
              </div>
          </div>
        </div>
      </div>
  </div>
  <!-- END modal Lock-->

  <!-- result modal -->
  <div class="data-modal modal fade" id="result-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="d-block">
          <button type="button" class="close pb-1 px-1 m-2 border shadow" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="pt-3 text-center">
            <h5>RESULT</h5>
        </div>
        <div class="modal-body p-3">
            <div class="text-center">
              <div class="spinner-border text-primary"></div>
              <div class="text-secondary">Loading...</div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END result modal -->

  <!-- loading modal -->
  <div class="modal fade mt-5" role="dialog" id="loading-modal" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-sm">
          <div class="modal-content shadow" >
            <div class="modal-body p-3">
              <div class="text-center">
                <div class="spinner-border text-primary"></div>
                <div class="text-secondary">Loading...</div>
              </div>
            </div>
          </div>
      </div>
  </div>
  <!-- END loading modal -->
</body>
</html>