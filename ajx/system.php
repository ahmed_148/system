<?php 
require '../class/db.php';
require '../class/model.php';
require '../class/tables.php';
require '../class/system.php';

// start if is set post //
if (isset($_POST['action'])){
	$action = $_POST['action'];
}else{
	exit('error: missing action!');
}
// end post//

// start action link //
if($action == 'link'){
	$id = model::secure($_POST['id']);
	systemClass::link($id);
}
// end action link //

// start action modal show //
if($action == 'modalShow'){
	$id = model::secure($_POST['id']);
	systemClass::modalShow($id);
}
// end action modal show //

// start action json modal //
if($action == 'jsonModal'){
	$id = model::secure($_POST['id']);
	systemClass::jsonModal($id);
}
// end action json modal //

// start action table //
if($action == 'test-table'){
$draw = model::secure($_POST["draw"]);
$start  = model::secure($_POST["start"]);//Paging first record indicator.
$length = model::secure($_POST['length']);//Number of records that the ta
$test = test::all();
$recordsTotal = $recordsFiltered = test::count_all();
$data = array();
if($test){
	foreach ($test as $key => $value) {
		$id = $value['id'];
		$value['update'] = "<a onclick='modalShow(\"system.php\",{action:\"form-update\",id:\"$id\"})' href='#'>Update</a>";

		$data[] = $value ;
	}
}
/* Response to client before JSON encoding */
$response = array(
	"draw" => intval($draw),
	"recordsTotal" => $recordsTotal,
	"recordsFiltered" => $recordsFiltered,
	"data" => $data
);
echo json_encode($response);  
}
// end action table //

// start form add //
if ($action == 'test-form-add'){
    $name = model::secure($_POST['name']);
    systemClass::formAddSubmit($name);
}
// end form add //

// start form update //
if ($action == 'form-update'){
    $id = model::secure($_POST['id']);
    systemClass::formUpdate($id);
}
// end form update //

// start form update //
if ($action == 'test-form-update'){
    $id = model::secure($_POST['id']);
    $name = model::secure($_POST['name']);
    systemClass::formUpdateSubmit($id,$name);
}
// end form update //