<?php

class systemClass extends model{

	// start link //
	public static function link($id){
		// start json notification //
		$json['notification'] = array('type'=>'success', 'msg'=>'Hello this is a link!');
		// end json notification //

		exit(json_encode($json, JSON_PRETTY_PRINT));
	}
	// end link //

	// start modal Show //
	public static function modalShow($id){
		echo "Hello this is modal";
	}
	// end modal Show //

	// start json modal //
	public static function jsonModal($id){

		// start json modal //
		$json['modal'] = "Hello this is json modal";
		// end json modal //

		exit(json_encode($json, JSON_PRETTY_PRINT));
	}
	// end json modal //

	// start table //
	public static function testTable(){
		?>
		<div class="table-responsive">
			<table class="table test text-center">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Update</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		<script type="text/javascript">
			$(function () {
				$('.test').DataTable({
					"aaSorting": [],
					"columns": [
					{"data": "id"},
					{"data": "name"},
					{"data": "update"}
					],
					"ordering": false,
					"processing": true,
					"serverSide": true,
					"ajax": {
						url: 'ajx/system.php',
						data: {action:'test-table'},
						type: 'POST'
					}
				});
			});
		</script>
		<?php
	}
	// end table //

	// start form add //
	public static function formAdd(){		
		?>
		<form method="post" onsubmit="submitForm(this, 'system.php')" prevent-default>
			<input type="hidden" name="action"  value="test-form-add" class="form-control">
			<div class="form-group">
				<label>Name:</label>
				<input type="text" class="form-control" name="name" placeholder="Name">
			</div>
			<button type="submit" class="btn btn-primary">Add</button>
		</form>
		<?php
	}
	// end form add //

	// start form add submit //
	public static function formAddSubmit($name){
		$x = array();
		$x['name'] = $name;
		$data = test::saveArray($x);
		if ($data) {
			// start json notification //
			$json['notification'] = array('type'=>'success', 'msg'=>'Add Name Successfully!');
			// start json notification //

			// start json redirect //
			$json['redirect'] = "index.php";
			// end json redirect //

			exit(json_encode($json, JSON_PRETTY_PRINT));
		}
		// start json notification //
		$json['notification'] = array('type'=>'danger', 'msg'=>'Something happened wrong!');
		// end json notification //

		// start json reload //
		$json['reload'] = true;
		// end json reload //

		exit(json_encode($json, JSON_PRETTY_PRINT));
	}
   // end form add submit //

	// start form update //
	public static function formUpdate($id){	
		$name = test::where('id',$id)['name'];	
		?>
		<form method="post" onsubmit="submitForm(this, 'system.php')" prevent-default>
			<input type="hidden" name="id" value="<?= $id ?>">
			<input type="hidden" name="action"  value="test-form-update" class="form-control">
			<div class="form-group">
				<label>Name:</label>
				<input type="text" class="form-control" name="name" value="<?= $name?>">
			</div>
			<button type="submit" class="btn btn-primary">Update</button>
		</form>
		<?php
	}
	// end form update //

	// start form update submit //
	public static function formUpdateSubmit($id,$name){
		$x = array();
		$x['name'] = $name;
		$data = test::updateArray($x,'id',$id);
		if ($data) {
			// start json notification //
			$json['notification'] = array('type'=>'success', 'msg'=>'Update Name Successfully!');
			// start json notification //

			// start json redirect //
			$json['redirect'] = "index.php";
			// end json redirect //

			exit(json_encode($json, JSON_PRETTY_PRINT));
		}
		// start json notification //
		$json['notification'] = array('type'=>'danger', 'msg'=>'Something happened wrong!');
		// end json notification //

		// start json reload //
		$json['reload'] = true;
		// end json reload //

		exit(json_encode($json, JSON_PRETTY_PRINT));
	}
   // end form update submit //

}